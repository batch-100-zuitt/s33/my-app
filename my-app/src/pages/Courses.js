import React, { Fragment, useContext, useState, useEffect } from 'react';
import UserContext from '../UserContext';
import Course from '../components/Course';
import coursesData from '../data/courses';
import { Link } from 'react-router-dom'


import {Button, Table, Nav} from 'react-bootstrap';

export default function Courses(){
	//use the UserContext and destructure it to access the user state defined
	//in App component
	const { user } = useContext(UserContext);

	const [isEnabled,setIsEnabled] = useState(true)
	const [isDisabled,setIsDisabled] = useState(false)
	const [courseStatus,setCourseStatus] = useState(true)


	

	useEffect (() => {

		if( isEnabled === true){
			setCourseStatus(true)

		}else{
			setCourseStatus(false)
			alert("Activated")
		}

	});



	const courses = coursesData.map(course => {
		if(course.onOffer){
			return (
				<Course key={course.id} course = {course} />
			);

		} else {
			return null;
		}
	})



	//table rows to be rendered in a bootstrap table when an admin is logged in
	const coursesRows = coursesData.map(course => {

			


	

		return (

			<tr key={course.id}>
				<td>{course.id}</td>
				<td>{course.name}</td>
				<td>Php {course.price}</td>
				<td>{course.onOffer ? 'open' : 'closed'}</td>
				<td>{course.start_date}</td>
				<td>{course.end_date}</td>
				<td>
					<Button variant="warning col-12 " as={Link} to="/Update">
					Update  
					</Button>	


					{courseStatus ?

					<Button variant="success col-12"
					 value={isEnabled}
                      onClick={(e) => setIsEnabled(e.target.isEnabled)}
					> Activate</Button>
					:
					<Button variant="danger col-12"
					value={isDisabled}
                    onCclick={(e) => setIsDisabled(e.target.true)}
					 > Deactivate
					</Button>
				

					}

				</td>
			</tr>

		);
	})

	return (
		user.isAdmin === true
		? 		
		<Fragment>
		
			<h1>Courses Dashboard</h1>
			<Table>					
					<th>ID</th>
					<th>Name</th>
					<th>Price</th>
					<th>Status</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Action Taken</th>
			
				<tbody>
					{coursesRows}
				</tbody>
			</Table>
		</Fragment>

		:

		<Fragment>
			{courses}
		</Fragment>
	)

}