import React,{useState} from 'react';
import { Button,Container, Form, Row,Col} from 'react-bootstrap';






export default function Addcourse (){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [onOffer, setOnOffer] = useState('');
	const [startdate, setStartdate] = useState('');
	const [enddate, setEnddate] = useState('');
	
		function AddCourse(e) {
		e.preventDefault();
			setName("")
			setDescription("")
			setPrice("")
			setOnOffer("")
			setStartdate("")
			setEnddate("")
		
		alert('Course Added!');
	   }

return(
		<Row>
			<Col>
				<Container>

					<Form onSubmit={(e) => AddCourse(e)}>
					<h1>Add Course</h1>
					<br />
					<Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Input Course Name" 
                        value = {name} 
                        onChange={e => setName(e.target.value)}
						required
                        required
                    />
                	</Form.Group>
																
					<Form.Group>
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Input Description" 
                        value = {description} 
                        onChange={e => setDescription(e.target.value)}
                        required
                    />
                	</Form.Group>
									
					<Form.Group>
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Input Price"
                        value = {price} 
                        onChange={e => setPrice(e.target.value)} 
                        required
                        />
                	</Form.Group>
										
					<Form.Group>
                    <Form.Label>OnOffer</Form.Label>
                    <Form.Control 
                        type="Boolean" 
                        placeholder="True/False"
                        value = {onOffer} 
                        onChange={e => setOnOffer(e.target.value)} 
                        required
                        />
                	</Form.Group>

                	<Form.Group>
                    <Form.Label>Start Date</Form.Label>
                    <Form.Control 
                        type="Date" 
                        placeholder=""
                        value = {startdate} 
                        onChange={e => setStartdate(e.target.value)} 
                        required
                    />
                	</Form.Group>

                	<Form.Group>
                    <Form.Label>End Date</Form.Label>
                    <Form.Control 
                        type="Date" 
                        placeholder="" 
                        value = {enddate} 
                        onChange={e => setEnddate(e.target.value)}
                        required
                     />
                	</Form.Group>

                	<Button variant="primary" type="submit" id="submitBtn">
                        Add Course
                    </Button>

					</Form>				
				</Container>
			</Col>
		</Row>



	)

}