import React,{ useState } from 'react';
import { Button,Container, Form, Row,Col} from 'react-bootstrap';




export default function Updatecourse (){

	const [editedname, setEditedName] = useState('');
	const [editeddescription, setEditedDescription] = useState('');
	const [editedprice, setEditedPrice] = useState('');
	const [editedonOffer, setEditedOnOffer] = useState('');
	const [editedstartdate, setEditedStartdate] = useState('');
	const [editedenddate, setEditedEnddate] = useState('');
	
		function EditCourse(e) {
		e.preventDefault();
			setEditedName("")
			setEditedDescription("")
			setEditedPrice("")
			setEditedOnOffer("")
			setEditedStartdate("")
			setEditedEnddate("")
		
		alert('Course Updated!');
	   }

return(
		<Row>
			<Col>
				<Container>

					<Form onSubmit={(e) => EditCourse(e)}>
					<h1>Update Course</h1>
					<br />
					<Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Input Course Name" 
                        value = {editedname} 
                        onChange={e => setEditedName(e.target.value)}
						required                      
                    />
                	</Form.Group>
																
					<Form.Group>
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Input Description" 
                        value = {editeddescription} 
                        onChange={e => setEditedDescription(e.target.value)}
                        required
                    />
                	</Form.Group>
									
					<Form.Group>
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Input Price"
                        value = {editedprice} 
                        onChange={e => setEditedPrice(e.target.value)} 
                        required
                        />
                	</Form.Group>
										
					<Form.Group>
                    <Form.Label>OnOffer</Form.Label>
                    <Form.Control 
                        type="Boolean" 
                        placeholder="True/False"
                        value = {editedonOffer} 
                        onChange={e => setEditedOnOffer(e.target.value)} 
                        required
                        />
                	</Form.Group>

                	<Form.Group>
                    <Form.Label>Start Date</Form.Label>
                    <Form.Control 
                        type="Date" 
                        placeholder=""
                        value = {editedstartdate} 
                        onChange={e => setEditedStartdate(e.target.value)} 
                        required
                    />
                	</Form.Group>

                	<Form.Group>
                    <Form.Label>End Date</Form.Label>
                    <Form.Control 
                        type="Date" 
                        placeholder="" 
                        value = {editedenddate} 
                        onChange={e => setEditedEnddate(e.target.value)}
                        required
                     />
                	</Form.Group>

                	<Button variant="primary" type="submit">
                        Update Course
                    </Button>

					</Form>				
				</Container>
			</Col>
		</Row>



	)

}