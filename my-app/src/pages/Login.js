import React, { useState, useEffect, useContext} from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import userData from '../data/users';




export default function Login() {
	const { user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);


    const [ willRedirect, setWillRedirect] = useState(false);



	function authenticate(e) {
		e.preventDefault();

        //Authentication based on imported users data
        const match = userData.find( user => {
            return (user.email === email && user.password === password);
        })

        if(match){
            localStorage.setItem('email',email);
            localStorage.setItem('isAdmin',match.isAdmin);
        

        setUser({
            email: localStorage.getItem('email'),
            isAdmin: match.isAdmin
        });
        setWillRedirect(true);
        } else {
            alert('Authentication failed, no match found.')
        }




		setEmail('');
		setPassword('');

        //set the email of the authentiated user in the local storage

		alert(`${email} has been verified! Welcome back!`);

	}
	useEffect(() => {

        // Validation to enable submit button when all fields are populated
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);


    useEffect(() => {
        console.log(`user with an email: ${email} is and admin $user.admin`);
    }, [user.isAdmin, user.email]);

    return (
        willRedirect === true ?
            <Redirect to = '/course' />
            :
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                         />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                        />
                </Form.Group>

                {isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </Form>
    )
}